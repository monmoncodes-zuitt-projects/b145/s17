console.log("Hello from JS"); //message

// [SECTION] CONTROL STRUCTURES

// [SUB SECTION]: IF-ELSE STATEMENT

let numA = 3; //we will try to assess the value.

// If statement (Branch)
// The task of the if statement is to execute a procedure/action if the specified condition is "true".

if (numA <= 0) {
   //truthy branch
   // this block of code will run if the condition is MET.
   console.log('The condition was MET!');
}

let name = 'Lorenzo';

// create a control structure that will allow the user to proceed if the value matches/passes the condition.
if (name === 'lorenzo') {
   // if it passes the condition, the "truthy" branch will run
   console.log ('User can proceed!'); //!pass -> JS is case-sensitive
}

// [SUB SECTION] Else Statement

// => This executed a statement if ALL other conditions are "FALSE" and/or has failed.

// let's create a control structure that will allow us to simulate a user login.

// prompt box -> (prompt): this will allow us to display a prompy dialog box which we can the user for an input.

// syntax: prompt(text/message[REQUIRED], placeholder/default text [OPTIONAL]);
// prompt("Please enter your first name:", "Martin");

// let's create a control structure that allows us to check if the user is old enough to drink
let age = 21;

// alert() => display a message to the user which would require their attention
// if (age >= 18) {
//    // "truthy" branch
//    alert("You're old enough to drink");
// } else {
//    // "falsy" branch
//    alert("Come back another day!");
// }


// let's create a control structure that will ask for the number of drinks that you will order.

// ask the user how many drinks he wants

let order = prompt('How many orders of drinks do you like?') //provides input field for user

// convert the string data type into a number.
// parseInt() => will allow us to convert strings into integers
// order = parseInt(order);

// create a logic that will make sure that the user's input is greater than 0

// Type coercion => conversion data type was converted to another data type.

// 1. if one of the operands is an object, it will be converted into a primitive data type (str, number, boolean).
// 2. if at least 1 operand is a string, it will convert the other operand into a string as well.
// 3. if both numbers are numbers, then an arithmetic operation will be executed.

// in JS, there are 3 ways to multiply a string.
   // 1. repeat() method => this will allow us to return a new string value that contains the number of copies of the string.
      // syntax: str.repeat(value/number)
   // 2. loops => for loop
   // 3. loops => while loop method

let drinksOrdered = "🍸";

if (order > 0) {
   console.log(typeof order);
   alert('Number of drinks you ordered: ' + drinksOrdered.repeat(order)); //multiplication
} else {
   alert('The number should be above 0');
}

// You want to create other predetermined conditions you can create a nested (multiple) if-else statement
